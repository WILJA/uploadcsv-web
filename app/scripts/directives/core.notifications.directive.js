/**
* @namespace Notifications
* @memberof Core
*/
define(['angularAMD', 'bootstrap'], function (angularAMD) {
    'use strict';
    angularAMD.directive('notifications', ['$timeout', '$rootScope', function ($timeout, $rootScope) {
        return {
            templateUrl: 'views/crud/partial.status_msg.html',
            controller: ['$scope', '$rootScope', function ($scope, $rootScope) {
                /**
                 *
                 * @memberof Core.Notifications
                 * @param  {type} msg
                 * @param  {type} type
                 * @param  {type} time
                 * @return {type}
                 */
                function showStatus(msg, type, time) {
                    $scope.statusdiv = angular.element('.status_msg');
                    $scope.statusdiv.parent().show();
                    $scope.status_message = msg;
                    $scope.statusdiv.removeClass('__hidden');
                    if (type === 'error') {
                        $scope.status_msg_class = '__error';
                    }
                    else if (type === 'success') { 
                        $scope.status_msg_class = '__success';
                    } else if (type === 'warning') { 
                    	$scope.status_msg_class = '__warning';
                    }

                    // var newwidth = $('notifications').parent().width();
                    // if(newwidth === 0 && $('notifications').parent().length > 1){
                    //     newwidth = $($('notifications').parent()[1]).width();
                    // }

                    // $('notifications').width(newwidth);

                    hideStatus(time);
                }

                var timeout_;
                /**
                 *
                 * @memberof Core.Notifications
                 * @param  {type} time
                 * @return {type}
                 */
                function hideStatus(time) {
                    clearTimeout(timeout_);
                    timeout_ = setTimeout(function () {
                        angular.element('.status_msg').addClass('__hidden');
                        $scope.status_msg_class = '';
                        $scope.$apply();
                        setTimeout(function(){
                            $('notifications').hide();
                        }, 200);
                    }, time);
                }

                $scope.notifications = {
                    show: showStatus,
                    hide: hideStatus
                };
            }]
        };
    }]);
});
