define(['angularAMD', 'app'], function (angularAMD, app) {

  'use strict';

  return app.config(['$stateProvider', '$provide', '$urlRouterProvider',

    function ($stateProvider, $provide, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/login');


      $stateProvider
        .state('home', angularAMD.route(
          {
            url: '/home',
            templateUrl: 'views/cliente/cliente.html',
            controllerUrl: 'controllers/cliente.controller'
          }))
        .state('login', angularAMD.route(
          {
            url: '/login',
            templateUrl: 'views/auth/login.html',
            controllerUrl: 'controllers/auth.controller'
          }))
        .state('cidade', angularAMD.route(
                {
                  url: '/cidade',
                  templateUrl: 'views/cidade/cidade.html',
                  controllerUrl: 'controllers/cidade.controller'
            }));
    }]);

});
