define(['run', 'factory/cidade.factory'], function() {

    'use strict';

    return ['$scope', '$rootScope', '$stateParams', '$state', 'BaseController', 'Cidade',
        function($scope, $rootScope, $stateParams, $state, BaseController, Cidade) {

          console.log('Carregou controller');

            angular.extend($scope, BaseController);

            console.log('Cidade Controller Loaded');
            $scope.cidade = new Cidade();
            $scope.newcidade = new Cidade();

            $scope.page = 0;
            $scope.size = 10;
            $scope.sortOrder = 'desc';
            $scope.sortType = 'name';
            $scope.sort = 'name,asc';
            $scope.searchAll = '';

            $scope.fileUpload = null;

            $scope.getList = function(value) {
                if ($scope.searchAll !== null && $scope.searchAll !== '' && $scope.searchAll !== undefined) {
                    if (value === 'recalculatePageCounter') {
                        $scope.cidade.listAll($scope.page, $scope.size, $scope.sort, $scope.searchAll, recalculatePageCounter);
                    } else if (value === 'recalculatePages') {
                        $scope.cidade.listAll($scope.page, $scope.size, $scope.sort, $scope.searchAll, recalculatePages);
                    }
                } else {
                    if (value === 'recalculatePageCounter') {
                        $scope.cidade.list($scope.page, $scope.size, $scope.sort, recalculatePageCounter);
                    } else if (value === 'recalculatePages') {
                        $scope.cidade.list($scope.page, $scope.size, $scope.sort, recalculatePages);
                    }
                }
            };

            $scope.$on('$stateChangeSuccess', function(event, toState) {
                console.log('state Change Success');
                if (toState.name === 'cidade') {
                    $scope.getList('recalculatePages');
                    $scope.listCount();
                } else if (toState.name === 'cidade_edit') {
                    $scope.cidade.load($stateParams.id);
                }
            });

            $scope.$on('$viewContentLoaded', function() {
                console.log('View Content Loaded');
            });

            $scope.editCidade = function(id) {
              console.log(id);
                $scope.editingId = id;
                $scope.cidade.load(id, function(data) {
                    $scope.newcidade = data;
                    $scope.showAdd('update');
                });
            };

            $scope.listAll = function(value) {

                $scope.searchAll = value;
                $scope.getList('recalculatePages');

            };

            $scope.listCount = function() {

                $scope.cidade.listCount( function(data) {
                    $scope.listcountuf = data;

                });

            };

            $scope.delete = function(id) {
                $scope.cidade.delete(id, function(data, status) {
                    if (status === 200) {
                        showStatus('Cidade excluída com sucesso', 'success', 5000);
                        $scope.getList('recalculatePages');
                    } else {
                        showStatus('Erro: ' + data.message, 'error', 5000);
                    }
                });
            };

            var handleFileSelectOperation = function (evt) {
                var file = evt.currentTarget.files[0];
                var reader = new FileReader();
                reader.onload = function (evt) {
                    angular.element('.modal').modal('show');
                    $scope.$apply(function ($scope) {
                        $scope.attachedFileUrl = evt.target.result;
                    });
                };
                reader.readAsDataURL(file);
            };

            $scope.uploadImage = function () {
                var file = angular.element(document.getElementById('fileInput'));
                file.on('change', handleFileSelectOperation);

            };
            

            $scope.uploadcsv = function() {
             
          
                $scope.cidade.uploadcsv($scope.attachedFileUrl, function(data, status) {
                    if (status === 200) {
                        $scope.hideAdd();
                        showStatus('Cidade adicionada com sucesso', 'success', 5000);
                        $scope.getList('recalculatePages');
                        $scope.listCount();
                    } else {
                        showStatus('Erro: ' + data.message, 'error', 5000);
                    }
                });
            };

            $scope.save = function() {
                $scope.newcidade.customer = $rootScope.getCustomer();
                $scope.cidade.save($scope.newcidade, function(data, status) {
                    if (status === 200) {
                        $scope.hideAdd();
                        showStatus('Cidade adicionado com sucesso', 'success', 5000);
                        $scope.getList('recalculatePages');
                    } else {
                        showStatus('Erro: ' + data.message, 'error', 5000);
                    }
                });
            };

            $scope.update = function() {
              console.log($scope.newcidade);
                $scope.cidade.update($scope.newcidade, function(data, status) {
                    if (status === 200) {
                        $scope.hideAdd();
                        showStatus('Cidade atualizado com sucesso', 'success', 5000);
                        $scope.getList('recalculatePages');
                    } else {
                        showStatus('Erro: ' + data.message, 'error', 5000);
                    }
                });
            };

            $scope.saveCidade = function() {
              console.log('Chamou save:'+ $scope.editingId);
                if ($scope.editingId) {
                    $scope.update();
                } else {
                    $scope.save();
                }
            };

            $scope.changePage = function(page) {
                if (page === 'prev') {
                    $scope.page = $scope.page - 1;
                }
                if (page === 'next') {
                    $scope.page = $scope.page + 1;
                }
                if (typeof page === 'number') {
                    $scope.page = page - 1;
                }

                if ($scope.page < 0) {
                    $scope.page = 0;
                }
                if ($scope.page > $scope.pageslength.length - 1) {
                    $scope.page = $scope.pageslength.length - 1;
                }

                console.log(page, $scope.page);

                $scope.getList('recalculatePageCounter');
            };



            $scope.changeSize = function (size) {
                $scope.size = size.size;
                $scope.sizesSelect = size;

                $scope.getList('recalculatePages');
            };

            $scope.changeSort = function (type) {
                if ($scope.sortOrder === 'desc') {
                    $scope.sortOrder = 'asc';
                } else {
                    $scope.sortOrder = 'desc';
                }
                $scope.sortType = type;

                $scope.sort = $scope.sortType + ',' + $scope.sortOrder;

                $scope.getList('recalculatePages');

            };

            $scope.sizes = [{
                name: '10 linhas',
                size: 10
            }, {
                name: '25 linhas',
                size: 25
            }, {
                name: '50 linhas',
                size: 50
            }, {
                name: '100 linhas',
                size: 100
            }];
            $scope.sizesSelect = $scope.sizes[0];

            $scope.showAdd = function() {
                hideStatus(0);
                $scope.templateURL = 'views/cidade/cidade_add.html';
                $scope.onLoadAdd();
            };

            $scope.onLoadAdd = function() {
                setTimeout(function() {
                    angular.element('.crud_add').removeClass('__hidden');
                    if ($scope.editingId) {
                        angular.element('.btn-delete').show();
                    } else {
                        angular.element('.btn-delete').hide();
                    }
                    $scope.$apply();
                }, 100);
            };

            $scope.hideAdd = function() {
                angular.element('.crud_add').addClass('__hidden');
                setTimeout(function() {
                    $scope.templateURL = '';
                    $scope.editingId = undefined;
                    $scope.newcidade = {};
                }, 200);
            };

            $scope.showexclusionmodal = function() {
                $scope.modalURL = 'views/crud/exclusion_modal.html';
                angular.element('.app-modal').removeClass('__hidden');
            };

            $scope.confirmExclusion = function() {
                $scope.modalURL = '';
                $scope.delete($scope.editingId);
                angular.element('.app-modal').addClass('__hidden');
                $scope.hideAdd();
            };

            $scope.cancelExclusion = function() {
                $scope.modalURL = '';
                angular.element('.app-modal').addClass('__hidden');
            };

            $scope.hasLogo = function(cidade) {
                if (cidade.logo === null || !cidade.logo) {
                    cidade.logo = 'images/company-placeholder.png';
                }
                return true;
            };

            function showStatus(msg, type, time) {
                $scope.statusdiv = angular.element('.status_msg');
                $scope.status_message = msg;
                $scope.statusdiv.removeClass('__hidden');
                if (type === 'error') {
                    $scope.status_msg_class = '__error';
                } else {
                    $scope.status_msg_class = '__success';
                }
                hideStatus(time);
            }

            function hideStatus(time) {
                setTimeout(function() {
                    angular.element('.status_msg').addClass('__hidden');
                    $scope.status_msg_class = '';
                    $scope.$apply();
                }, time);
            }

            $scope.changePage = function (page) {
                if (page === 'prev') {
                    $scope.page = $scope.page - 1;
                }
                if (page === 'next') {
                    $scope.page = $scope.page + 1;
                }
                if (typeof page === 'number') {
                    $scope.page = page - 1;
                }

                if ($scope.page < 0) {
                    $scope.page = 0;
                }
                if ($scope.page > $scope.realPageSize - 1) {
                    $scope.page = $scope.realPageSize - 1;
                }

                if ($scope.page - 5 > 0) {
                    $scope.pageslength = [];
                    for (var p = 0; p < 5; p += 1) {
                        $scope.pageslength.push($scope.page - p);
                    }
                    $scope.pageslength.reverse();
                    for (var a = 1; a < 6; a += 1) {
                        $scope.pageslength.push($scope.page + a);
                    }
                }
                else {
                    var l = $scope.pageslength.length;
                    $scope.pageslength = [];
                    for (var k = 0; k < l; k += 1) {
                        $scope.pageslength.push(k + 1);
                    }
                }

                $scope.getList('recalculatePageCounter');
            };

            function recalculatePages() {
                var pagesize;
                if ($scope.cidade.totalPages > 10) {
                    pagesize = 10;
                }
                else {
                    pagesize = $scope.cidade.totalPages;
                }
                $scope.realPageSize = $scope.cidade.totalPages;
                $scope.pageslength = new Array(pagesize);
                for (var c = 0; c < $scope.pageslength.length; c += 1) {
                    $scope.pageslength[c] = c + 1;
                }
                recalculatePageCounter();
            }

            function recalculatePageCounter() {
                var totalofpage = $scope.size * ($scope.page + 1);
                var paginationmin = 1 + ($scope.size * $scope.page);
                if (totalofpage > $scope.cidade.totalElements) {
                    totalofpage = $scope.cidade.totalElements;
                }
                $scope.totalElements = $scope.cidade.totalElements;
                $scope.pagesCounter = paginationmin + ' - ' + totalofpage;

                if ($scope.page + 1 > $scope.cidade.totalPages) {
                    $scope.changePage($scope.page);
                    return;
                }
            }


        }
    ];
});
